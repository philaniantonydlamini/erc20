import React, { useEffect } from 'react';
import { MoralisProvider, useMoralis } from 'react-moralis';

export default function Header() {
  const { enableWeb3, account, Moralis, isWeb3Enabled, isAuthenticated } = useMoralis();

  useEffect(() => {
    if (isAuthenticated && !isWeb3Enabled) {
      enableWeb3();
    }

    const handleChainChanged = (chainId) => {
      console.log(`Chain changed to ${chainId}`);
      // You can add logic here to handle the chain change
    };

    if (Moralis?.web3 && typeof Moralis.web3.on !== 'undefined') {
      Moralis.web3.on('chainChanged', handleChainChanged);
    }

    return () => {
      if (Moralis?.web3 && typeof Moralis.web3.off !== 'undefined') {
        Moralis.web3.off('chainChanged', handleChainChanged);
      }
    };
  }, [Moralis, isWeb3Enabled, isAuthenticated, enableWeb3]);

  return (
    <div>
      {account ? (
        <div>
          Connected to {account.slice(0, 6)}...{account.slice(-4)}
        </div>
      ) : (
        <button
          onClick={async () => {
            await enableWeb3();
          }}
        >
          Enable Web3
        </button>
      )}
    </div>
  );
}