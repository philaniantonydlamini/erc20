const hre = require('hardhat');

async function main() {
  // Get the contract factory for MyXCoin
  const Xcoin = await hre.ethers.getContractFactory("MyXCoin");

  // Deploy the contract
  const xcoin = await Xcoin.deploy("xcoin", "cx");

  // Wait for the contract to be deployed
  await xcoin.waitForDeployment();

  // Get the contract address
  console.log("Contract deployed to address:", xcoin.target);

  // Get the deployment transaction
  const tx = xcoin.deploymentTransaction();
  // Wait for the transaction receipt
  const receipt = await tx.wait();

  console.log("Contract deployed by address:", receipt.from);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });