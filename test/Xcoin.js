const { expect } = require('chai');
const { ethers } = require('hardhat');

describe('MyXCoin', function () {

  let xcoin;
  let owner;
  let otherAccount;

  beforeEach(async () => {
    [owner, otherAccount] = await ethers.getSigners();
    const Xcoin = await ethers.getContractFactory("MyXCoin");
    xcoin = await Xcoin.deploy("xcoin", "cx");
    //wait for conract to get deployed
    await xcoin.deploymentTransaction().wait();
  });

  describe('Deployment', function () {
    it('Should set correct token name and symbol', async function() {
      expect(await xcoin.name()).to.equal('xcoin');
      expect(await xcoin.symbol()).to.equal('cx');
    });

    it('Should assign the total supply of tokens to the owner', async function() {
      const ownerBalance = await xcoin.balanceOf(owner.address);
      expect(ownerBalance).to.equal(BigInt(1000000) * BigInt(10) ** BigInt(18));
    });
  });

});